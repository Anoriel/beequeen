﻿using UnityEngine;
using System.Collections;

public class HoneyComb : MonoBehaviour {

    int state;

    public Sprite empty, newComb, larvae, closed, ready, old;
    public HoneyCombMenu honeyCombMenu;

    void start()
    {
        state = 0; // 0 --> empty, 1 --> new, 2 --> larvae, 3 --> closed, 4 --> ready, -1 --> old
        
    }
    void OnMouseDown()
    {
        if (state < 0)
        {
            GetComponent<SpriteRenderer>().sprite = empty;
            state++;
        }
        else if (state == 0 && honeyCombMenu.IsActive)
        {
            GetComponent<SpriteRenderer>().sprite = newComb;
        }
        else if (state == 1)
        {
            GetComponent<SpriteRenderer>().sprite = larvae;
            state++;
        }
        else if (state == 2)
        {
            GetComponent<SpriteRenderer>().sprite = closed;
            state++;
        }
        else if (state == 3)
        {
            GetComponent<SpriteRenderer>().sprite = ready;
            state++;
        }
        else if (state == 4)
        {
            GetComponent<SpriteRenderer>().sprite = old;
            state = -1;
        }
    }

    public int getState
    {
        get { return state; }
    }
}
