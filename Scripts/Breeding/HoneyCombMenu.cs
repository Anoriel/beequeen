﻿using UnityEngine;
using System.Collections;

public class HoneyCombMenu : MonoBehaviour {

    bool honeyCombActive;
    

    void Start()
    {
        honeyCombActive = false;
    }

    void OnMouseDown()
    {
        print("Honeycomb active: " + honeyCombActive);
        if (honeyCombActive)
        {
            honeyCombActive = false;
        }else
        {
            honeyCombActive = true;
        }

    }

    public bool IsActive
    {
        get { return honeyCombActive; }
    }
}
