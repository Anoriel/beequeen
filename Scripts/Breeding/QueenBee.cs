﻿using UnityEngine;
using System.Collections;

public class QueenBee : MonoBehaviour {

    bool queenIsActive;
    Vector3 target;
    Vector3 rotation;
    Vector3 queenBeeStart;
    float angle;

    public GameObject queenBee;
    public float movingSpeed;
    public float rotationSpeed;

    void Start()
    {
        queenIsActive = false;
        queenBee.SetActive(queenIsActive);
        queenBeeStart = queenBee.transform.position;
        target = queenBee.transform.position;
        rotation = queenBee.transform.position;
        angle = 0.0f;
    }

    void FixedUpdate()
    {
        //Vector3 rotDir = new Vector3(0, 0, 0);
        target.z = 0.0f;
        if (Input.GetMouseButtonDown(0) && queenIsActive)
        {
            target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //Vector3 viewDir = Vector3.RotateTowards(queenBee.transform.position, target, 0.0f, 0.0f);
            //rotation = target - queenBee.transform.position;

            //queenBee.transform.RotateAround(queenBee.transform.position, new Vector3(0, 0, 1), 1.5f);
            //queenBee.transform.rotation = Quaternion.LookRotation(viewDir);
            //angle = Quaternion.FromToRotation(Vector3.forward, transform.forward).eulerAngles.y;

            //rotDir = queenBee.transform.position - target;
            //angle = Mathf.Atan2(rotDir.y, rotDir.x);
            //angle = Quaternion.FromToRotation(-queenBee.transform.position, queenBee.transform.position-target).eulerAngles.y;
            //print("Angle: " + angle);
            
        }

        //queenBee.transform.Rotate(new Vector3(0, 0, 1), angle * Time.deltaTime * rotationSpeed);
        queenBee.transform.position = Vector3.MoveTowards(queenBee.transform.position, target, movingSpeed * Time.deltaTime);
        //queenBee.transform.rotation = Quaternion.Slerp(queenBee.transform.rotation, Quaternion.LookRotation(rotation), rotationSpeed * Time.deltaTime);
    }

    void OnMouseDown()
    {
        print("Someone clicked the bee");
        if (queenIsActive)
        {
            queenIsActive = false;
        }
        else
        {
            queenIsActive = true;
        }
        
        queenBee.SetActive(queenIsActive);
    }
}
