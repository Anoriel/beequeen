﻿using UnityEngine;
using System.Collections;

public class chFlower : MonoBehaviour {

    public float honey;
    public bool collected = false;
    public eFlowerType type;

    public Color pollenColor;

    Vector3 initialScale;

    void Awake()
    {
        initialScale = transform.localScale;
    }

    
	// Update is called once per frame
	public void Collect (float multiplier) {
        if (!collected)
        {
            collected = true;
            GetComponent<Collider2D>().enabled = false;
            StartCoroutine(Shrink());
            CollectHoneyManager.singleton.AddHoney(honey * multiplier);
        }
	}

    IEnumerator Shrink()
    {
        float timer = 1f;
        while (timer > 0)
        {
            timer = Mathf.Clamp01(timer-Time.deltaTime * CollectHoneyManager.singleton.flowerShrinkTime);
            transform.localScale = initialScale * timer;
            yield return null;
        }
        Destroy(gameObject);
            
    }
}

public enum eFlowerType
{
    none,
    loewenzahn,
    rose,
    gaensebluemchen,
    klee,
    random
}
