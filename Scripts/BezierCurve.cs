﻿using UnityEngine;

public class BezierCurve : MonoBehaviour
{

    public Vector3[] points;

    public Vector3 GetPoint(float t)
    {
        return transform.TransformPoint(Bezier.GetPoint(points[0], points[1], points[2], points[3], t));
    }

    public Vector3 GetVelocity(float t)
    {
        return transform.TransformPoint(
            Bezier.GetFirstDerivative(points[0], points[1], points[2], points[3], t)) - transform.position;
    }

    public void Reset()
    {
        points = new Vector3[] {
            new Vector3(1f, 0f, 0f),
            new Vector3(2f, 0f, 0f),
            new Vector3(3f, 0f, 0f),
            new Vector3(4f, 0f, 0f)
        };
    }

    public Vector3 GetDirection(float t)
    {
        return GetVelocity(t).normalized;
    }

    public Vector3 QuadMiddlepoint
    {
        get
        {
            return (3 * points[2] - points[3] + 3 * points[1] - points[0]) / 4;
        }
    }

    public double GetLength()
    {
        Vector3 qm = QuadMiddlepoint;
        Vector3 A0 = qm - points[0];
        Vector3 A1 = points[0] - 2 * qm + points[3];
            if (A1.magnitude != 0)
            {
                float c = 4 * TMath.VectorComponentSum(Vector3.Scale(A1, A1));
                float b = 8 * TMath.VectorComponentSum(Vector3.Scale(A0, A1));
                float a = 4 * TMath.VectorComponentSum(Vector3.Scale(A0, A0));
                float q = 4 * a * c - b * b;
                float twoCpB = 2 * c + b;
                float sumCBA = c + b + a;
                double mult0 = 0.25 / c;
                double mult1 = q / (8 * Mathf.Pow(c, 1.5f));
                return
                    mult0 * (twoCpB * Mathf.Sqrt(sumCBA) - b * Mathf.Sqrt(a)) +
                    mult1 * (Mathf.Log(2 * Mathf.Sqrt(c * sumCBA) + twoCpB) - Mathf.Log(2 * Mathf.Sqrt(c * a) + b));
            }
            else return 2 * A0.magnitude;
      }
    
}