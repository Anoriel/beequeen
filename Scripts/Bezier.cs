﻿using UnityEngine;

public static class Bezier
{
    public static Vector3 GetPoint(Vector3 p0, Vector3 p1, Vector3 p2, float t)
    {
        t = Mathf.Clamp01(t);
        float oneMinusT = 1f - t;
        return
            oneMinusT * oneMinusT * p0 +
            2f * oneMinusT * t * p1 +
            t * t * p2;
    }

    public static Vector3 GetFirstDerivative(Vector3 p0, Vector3 p1, Vector3 p2, float t)
    {
        return
            2f * (1f - t) * (p1 - p0) +
            2f * t * (p2 - p1);
    }

    public static Vector3 GetPoint(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
    {
        t = Mathf.Clamp01(t);
        float oneMinusT = 1f - t;
        return
            oneMinusT * oneMinusT * oneMinusT * p0 +
            3f * oneMinusT * oneMinusT * t * p1 +
            3f * oneMinusT * t * t * p2 +
            t * t * t * p3;
    }

    public static Vector3 GetFirstDerivative(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
    {
        t = Mathf.Clamp01(t);
        float oneMinusT = 1f - t;
        return
            3f * oneMinusT * oneMinusT * (p1 - p0) +
            6f * oneMinusT * t * (p2 - p1) +
            3f * t * t * (p3 - p2);
    }

    public static Vector3 QuadraticMiddlepoint(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        return (3 * p2 - p3 + 3 * p1 - p0) / 4;
    }

    public static float GetQuadraticLength(Vector3 p0, Vector3 p1, Vector3 p2)
    {
        Vector3 A0 = p1 - p0;
        Vector3 A1 = p0 - 2 * p1 + p2;
        if (A1.magnitude != 0)
        {
            float c = 4 * TMath.VectorComponentSum(Vector3.Scale(A1, A1));
            float b = 8 * TMath.VectorComponentSum(Vector3.Scale(A0, A1));
            float a = 4 * TMath.VectorComponentSum(Vector3.Scale(A0, A0));
            float q = 4 * a * c - b * b;
            float twoCpB = 2 * c + b;
            float sumCBA = c + b + a;
            float mult0 = 0.25f / c;
            float mult1 = q / (8 * Mathf.Pow(c, 1.5f));
            return
                mult0 * (twoCpB * Mathf.Sqrt(sumCBA) - b * Mathf.Sqrt(a)) +
                mult1 * (Mathf.Log(2 * Mathf.Sqrt(c * sumCBA) + twoCpB) - Mathf.Log(2 * Mathf.Sqrt(c * a) + b));
        }
        else return 2 * A0.magnitude;
    }
}