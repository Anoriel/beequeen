﻿using UnityEngine;
using System.Collections;

public class FlowerSpawner : MonoBehaviour {

    public chFlower[] flowerPrefabs;
    public LayerMask flowerLayer, beeLayer;
    
    // Use this for initialization
	void Start () {
        StartCoroutine(SpawnFlowers());
    }
	
	// Update is called once per frame
	IEnumerator SpawnFlowers ()
    {
	    while (CollectHoneyManager.singleton.remainingTime > 0)
        {
            
            bool positionValid = false;
            Vector2 newPos = Vector2.zero;
            Collider2D result;
            while (!positionValid)
            {
                newPos = new Vector2(Random.Range(-4f, 4f), Random.Range(-2.1f, 2.1f));
                result = Physics2D.OverlapCircle(newPos, CollectHoneyManager.singleton.flowerSpawnRadius, flowerLayer);
                if (result == null)
                    result = Physics2D.OverlapCircle(newPos, CollectHoneyManager.singleton.flowerBeeSpawnDistance, beeLayer);
                if (result == null)
                positionValid = true;
                yield return null;
            }
            chFlower newFlower = Instantiate(flowerPrefabs[Random.Range(0, flowerPrefabs.Length)]);
            newFlower.transform.position = newPos;
            
            yield return new WaitForSeconds(CollectHoneyManager.singleton.flowerSpawnRate);
        }
	}

    public void SpawnFlower(eFlowerType type)
    {
        chFlower newFlower = null;
        switch (type)
        {
            case eFlowerType.gaensebluemchen: newFlower = Instantiate(flowerPrefabs[0]); break;
            case eFlowerType.loewenzahn: newFlower = Instantiate(flowerPrefabs[1]); break;
            case eFlowerType.rose: newFlower = Instantiate(flowerPrefabs[2]); break;
            case eFlowerType.klee: newFlower = Instantiate(flowerPrefabs[3]); break;
            case eFlowerType.random: newFlower = Instantiate(flowerPrefabs[Random.Range(0, flowerPrefabs.Length)]); break;
        }
        
        bool positionValid = false;
        Vector2 newPos = Vector2.zero;
        Collider2D result;
        while (!positionValid )
        {
            newPos = new Vector2(Random.Range(-4f, 4f), Random.Range(-2.1f, 2.1f));
            result = Physics2D.OverlapCircle(newPos, CollectHoneyManager.singleton.flowerSpawnRadius, flowerLayer);
            if (result == null)
                result = Physics2D.OverlapCircle(newPos, CollectHoneyManager.singleton.flowerBeeSpawnDistance, beeLayer);
            if (result == null)
                positionValid = true;
        }

        newFlower.transform.position = newPos;
    }
}
