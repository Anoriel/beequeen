﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class chBeeMovement : MonoBehaviour {

    private LineRenderer path;
    private List<Vector3> pathPoints;
    public Vector2 movementDirection;
    public SpriteRenderer pollenPackage;
    private float pollenAmount = 0;

    private Rigidbody2D rigid;

    public float stamina, pathStamina;

    public BezierSpline pathSpline;
    float timer = 0, collectTimer = 0;
    bool hasPath = false, flyingHome = false;
    int pathPointCount;

    public TextMesh pointTextPrefab;

    float comboMultiplier = 1;

    float pathLength;

    private chFlower currentFlower;
    public eFlowerType lastFlower;
    
    // Use this for initialization
	void Awake () {
        pathPoints = new List<Vector3>();
        path = GetComponent<LineRenderer>();
        rigid = GetComponent<Rigidbody2D>();
        //pointText.GetComponent<Renderer>().sortingLayerName = "ui";
    }
    void Start () {
        stamina = CollectHoneyManager.singleton.beeStamina;
        pathStamina = stamina;
        path.sortingLayerName = "path";
        pathPoints.Clear();
        pathPoints.Add(transform.position);
    }

    public void AddPathPoint(Vector3 newPoint)
    {

        
        if (pathStamina > 0)
        {
            float previousPathLength = pathLength;
            pathSpline.AddCurve();
            pathSpline.SetControlPoint(pathSpline.ControlPointCount - 1, newPoint);
            pathSpline.SetControlPoint(pathSpline.ControlPointCount - 2, newPoint - (newPoint - pathPoints[pathPoints.Count - 1]) * 0.25f);
            //pathSpline.SetControlPoint(pathSpline.ControlPointCount - 3, pathPoints(newPoint - pathPoints[pathPoints.Count - 2]) + (newPoint-pathPoints[pathPoints.Count - 1]) * 0.25f);

            if (pathPoints.Count > 2)
            {
                pathSpline.SetControlPointMode(pathSpline.ControlPointCount - 4, BezierControlPointMode.Mirrored);
                Vector3 tangentDirection = (newPoint - pathPoints[pathPoints.Count - 2]).normalized;
                pathSpline.SetControlPoint(pathSpline.ControlPointCount - 3, pathPoints[pathPoints.Count - 1] +
                    tangentDirection * (newPoint - pathPoints[pathPoints.Count - 1]).magnitude * 0.25f);
                //Mathf.Abs(TMath.DistanceInDirection(pathPoints[pathPoints.Count - 2], pathPoints[pathPoints.Count - 1], tangentDirection)));
            }
            else
                pathSpline.SetControlPoint(pathSpline.ControlPointCount - 3, pathPoints[pathPoints.Count - 1] + (newPoint - pathPoints[pathPoints.Count - 1]) * 0.25f);
            pathPoints.Add(newPoint);
            pathLength = pathSpline.GetLength();

            pathStamina -= (pathLength - previousPathLength);

            RedrawLine();
        }
    }
	
	// Update is called once per frame
	void OnMouseDown () {
        CollectHoneyManager.singleton.SelectBee(this);
	}

    void OnTriggerEnter2D(Collider2D otherCollider)
    {
        if ((otherCollider.tag == "Bee") && (!flyingHome) && (!otherCollider.GetComponent<chBeeMovement>().flyingHome))
        {
            FlyHome();
            otherCollider.GetComponent<chBeeMovement>().FlyHome();
        }
        else if (otherCollider.tag == "Flower")
        {
            currentFlower = otherCollider.GetComponent<chFlower>();
        }
    }

    void FlyHome()
    {
        flyingHome = true;
        GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.5f);
        GetComponent<Collider2D>().enabled = false;
        path.enabled = false;
        movementDirection = Vector2.up;
        transform.rotation = Quaternion.identity;
    }

    public void ResetPath(Vector3 newPos)
    {
        pathPoints.Clear();
        pathPoints.Add(transform.position);
        pathPoints.Add(newPos);
        pathSpline.Reset(transform.position, 
            transform.position + (newPos-transform.position)*0.25f, 
            newPos - (newPos - transform.position) * 0.25f, 
            newPos);
        RedrawLine();
        hasPath = true;
        pathLength = pathSpline.GetLength();
        timer = 0;
        pathStamina = stamina;
    }

    void OnMouseUp()
    {
        CollectHoneyManager.singleton.selectedBee = null;
    }

    void RedrawLine()
    {
        pathPointCount = Mathf.Clamp(Mathf.RoundToInt(pathLength * CollectHoneyManager.singleton.lineRendererPointsPerPathLength), 1, 100000);
        float cutOffLength = 0;
        List <Vector3> linePoints = new List<Vector3>();
        for (int i = 0; i < pathPointCount; i++)
        {
            if (pathLength / pathPointCount * i >= timer)
                linePoints.Add(pathSpline.GetPointAtDistance(pathLength / pathPointCount * i));
            else
                cutOffLength += pathLength / pathPointCount;
        }
        path.SetVertexCount(linePoints.Count);
        path.material.mainTextureScale = new Vector2((pathLength - cutOffLength) * CollectHoneyManager.singleton.lineRendererPointsPerPathLength * CollectHoneyManager.singleton.pathTextureScale, 1);
        path.material.mainTextureOffset = new Vector2(cutOffLength * CollectHoneyManager.singleton.pathTextureLength, 0);
        path.SetPositions(linePoints.ToArray());
    }

    public void UpdateLastSplinePoint(Vector3 newPos, Vector2 currentDirection)
    {
        if (pathStamina > 0)
        {
            Debug.Log("0 " + pathSpline.ControlPointCount + " " + pathPoints.Count);
            float previousPathLength = pathLength;
            pathSpline.SetControlPoint(pathSpline.ControlPointCount - 1, newPos);
            pathSpline.SetControlPoint(pathSpline.ControlPointCount - 2, newPos - (newPos - pathPoints[pathPoints.Count - 1]) * 0.25f);
            //pathSpline.SetControlPoint(pathSpline.ControlPointCount - 3, pathPoints(newPoint - pathPoints[pathPoints.Count - 2]) + (newPoint-pathPoints[pathPoints.Count - 1]) * 0.25f);
            Debug.Log("1");
            if (pathPoints.Count > 2)
            {
                pathSpline.SetControlPointMode(pathSpline.ControlPointCount - 4, BezierControlPointMode.Mirrored);
                Vector3 tangentDirection = (newPos - pathPoints[pathPoints.Count - 2]).normalized;
                pathSpline.SetControlPoint(pathSpline.ControlPointCount - 3, pathPoints[pathPoints.Count - 1] +
                    tangentDirection * (newPos - pathPoints[pathPoints.Count - 1]).magnitude * 0.1f);
                //Mathf.Abs(TMath.DistanceInDirection(pathPoints[pathPoints.Count - 2], pathPoints[pathPoints.Count - 1], tangentDirection)));
            }
            else
                pathSpline.SetControlPoint(pathSpline.ControlPointCount - 3, pathPoints[pathPoints.Count - 1] + (newPos - pathPoints[pathPoints.Count - 1]) * 0.1f);
            Debug.Log("2");
            pathPoints[pathPoints.Count-1] = newPos;
            pathLength = pathSpline.GetLength();

            pathStamina -= (pathLength - previousPathLength);

            RedrawLine();
        }

        /*pathSpline.SetControlPoint(pathSpline.ControlPointCount - 1, newPos);
        //Vector2 tangentPos = 
        //    newPos + Mathf.Abs(TMath.DistanceInDirection(newPos, pathSpline.GetControlPoint(pathSpline.ControlPointCount-4), currentDirection)) * currentDirection;

        
        //pathSpline.SetControlPoint(pathSpline.ControlPointCount - 3, pathPoints(newPoint - pathPoints[pathPoints.Count - 2]) + (newPoint-pathPoints[pathPoints.Count - 1]) * 0.25f);

        if (pathPoints.Count > 2)
        {
            pathSpline.SetControlPoint(pathSpline.ControlPointCount - 2, newPos - (newPos - pathPoints[pathPoints.Count - 2]) * 0.25f);
            pathSpline.SetControlPointMode(pathSpline.ControlPointCount - 4, BezierControlPointMode.Mirrored);
            Vector3 tangentDirection = ((Vector3)newPos - pathPoints[pathPoints.Count - 2]).normalized;
            pathSpline.SetControlPoint(pathSpline.ControlPointCount - 3, pathPoints[pathPoints.Count - 2] +
                tangentDirection * ((Vector3)newPos - pathPoints[pathPoints.Count - 1]).magnitude * 0.25f);
            //Mathf.Abs(TMath.DistanceInDirection(pathPoints[pathPoints.Count - 2], pathPoints[pathPoints.Count - 1], tangentDirection)));
        }
        else
            pathSpline.SetControlPoint(pathSpline.ControlPointCount - 3, pathPoints[pathPoints.Count - 1] + (newPos - pathPoints[pathPoints.Count - 1]) * 0.25f);


        pathPoints[pathPoints.Count-1] = newPos;
        pathLength = pathSpline.GetLength();

        pathStamina -= (pathLength - previousPathLength);
        //pathSpline.SetControlPoint(pathSpline.ControlPointCount - 2, tangentPos);
        //pathSpline.SetControlPoint(pathSpline.ControlPointCount - 3, tangentPos);
        pathLength = pathSpline.GetLength();
        RedrawLine();*/
    }


    void Update()
    {
        if ((stamina <= 0) && !flyingHome)
            FlyHome();

        if (flyingHome)
        {
            rigid.MovePosition((Vector2)transform.position + Vector2.up * Time.deltaTime * CollectHoneyManager.singleton.beeSpeed);
        }
        else if (collectTimer > 0)
        {
            collectTimer -= Time.deltaTime;
            if (collectTimer < 0.33f)
                GetComponent<Animator>().SetBool("Collecting", false);
            if (collectTimer <= 0)
            {
                if (lastFlower == currentFlower.type)
                {
                    comboMultiplier++;
                    if (comboMultiplier > 2 && comboMultiplier%2 != 0)
                        CollectHoneyManager.singleton.flowerSpawner.SpawnFlower(currentFlower.type);
                }
                else
                    comboMultiplier = 1;
                currentFlower.Collect(comboMultiplier);
                pollenAmount += currentFlower.honey;
                pollenPackage.color = (pollenPackage.color + currentFlower.pollenColor)/2;
                UpdatePollenPackages();
                lastFlower = currentFlower.type;
                currentFlower = null;
            }
        }
        else if (hasPath)
        {
            if (currentFlower != null)
            {
                if (Vector2.Distance(currentFlower.transform.position, transform.position) <= CollectHoneyManager.singleton.collectHoneyDistance)
                {
                    collectTimer = Random.Range(CollectHoneyManager.singleton.collectHoneyMinDuration, CollectHoneyManager.singleton.collectHoneyMaxDuration);
                    GetComponent<Animator>().SetBool("Collecting", true);
                }
            }

            timer += Time.deltaTime * CollectHoneyManager.singleton.beeSpeed;

            //path.material.mainTextureOffset = new Vector2(-timer * 0.4f, 0);
            RedrawLine();

            movementDirection = (pathSpline.GetPointAtDistance(timer) - transform.position).normalized;
            stamina -= Time.deltaTime * CollectHoneyManager.singleton.beeSpeed;
            rigid.MovePosition(pathSpline.GetPointAtDistance(timer));
            transform.rotation = Quaternion.AngleAxis(Mathf.Atan2(movementDirection.y, movementDirection.x) * Mathf.Rad2Deg - 90f, Vector3.forward);
            if (timer >= pathLength)
                hasPath = false;
        }
        else
        {
            if (currentFlower != null) 
            {
                if (Vector2.Distance(currentFlower.transform.position, transform.position) <= CollectHoneyManager.singleton.collectHoneyDistance)
                {
                    collectTimer = Random.Range(CollectHoneyManager.singleton.collectHoneyMinDuration, CollectHoneyManager.singleton.collectHoneyMaxDuration);
                    GetComponent<Animator>().SetBool("Collecting", true);
                }
            }

            rigid.MovePosition((Vector2)transform.position + movementDirection * Time.deltaTime * CollectHoneyManager.singleton.beeSpeed);
            stamina -= Time.deltaTime * CollectHoneyManager.singleton.beeSpeed;

            transform.rotation = Quaternion.AngleAxis(Mathf.Atan2(movementDirection.y, movementDirection.x) * Mathf.Rad2Deg - 90f, Vector3.forward);
        }
            //Debug.Log("distance: " + timer + " point: " + transform.position);
        //}
        

    }

    void UpdatePollenPackages()
    {
        if (pollenAmount > 3)
        {
            pollenPackage.sprite = CollectHoneyManager.singleton.pollenPackages[0];
        }
        else if (pollenAmount > 5)
        {
            pollenPackage.sprite = CollectHoneyManager.singleton.pollenPackages[1];
        }
    }
}

