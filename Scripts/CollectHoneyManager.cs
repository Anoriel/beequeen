﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CollectHoneyManager : MonoBehaviour {

    public static CollectHoneyManager singleton;

    public float pathPointDistance;     // distance at which a new path point should be created
    public int lineRendererPointsPerPathLength = 10;
    public chBeeMovement selectedBee;  // bee that is dragged around

    public Sprite[] pollenPackages;

    public bool drawingPath;

    public float beeSpeed = 0.5f;
    public float beeStamina = 10f;
    public float flowerShrinkTime = 1f;
    

    public float timeLimit = 300f;
    public float remainingTime;
    public float pathTextureLength = 0.4f;     // path redrawing offset
    public float pathTextureScale = 1f;

    public float flowerSpawnRate = 3f;
    public float flowerSpawnRadius = 1;
    public float flowerBeeSpawnDistance = 2;
    public float beeSpawnRate = 12f;
    public float collectHoneyDistance = 0.2f, collectHoneyMinDuration = 0.2f, collectHoneyMaxDuration = 2f;

    public float honey;

    public Text uiTimer, uiHoney;

    public FlowerSpawner flowerSpawner;

    private Vector2 oldPos, newPos;
    
    void Awake () {
        singleton = this;
        remainingTime = timeLimit;
        StartCoroutine(Timer());
    }
	
	// Update is called once per frame
	
    public void SelectBee(chBeeMovement _selectedBee)
    {
        selectedBee = _selectedBee;
        oldPos = _selectedBee.transform.position;
        StartCoroutine(DrawPath());
    }

    public void AddHoney(float newHoney)
    {
        honey += newHoney;
        uiHoney.text = Mathf.RoundToInt(honey).ToString();
    }

    IEnumerator DrawPath()
    {
        drawingPath = true;
        bool pathIsReset = false;

        Vector2 currentDirection, lastFrameDirection, 
            lastControlPointPos = Vector2.zero, lastControlPointDirection = Vector2.zero;

        while (selectedBee != null)
        {
            if (Input.GetMouseButton(0))
            {
                newPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                
                if (Vector2.Distance(oldPos, newPos) > pathPointDistance)
                {
                    currentDirection = (newPos - oldPos).normalized;
                    if (!pathIsReset)
                    {
                        selectedBee.ResetPath(newPos);
                        pathIsReset = true;
                        lastControlPointPos = newPos;
                    }
                    else
                    {
                        if (oldPos == lastControlPointPos)
                            lastControlPointDirection = currentDirection;
                        // add new point if Wendepunkt or >= 90° curve
                        /*if (Vector2.Angle(currentDirection, lastControlPointDirection) >= 80f)
                        {
                            selectedBee.AddPathPoint(newPos);
                            lastControlPointDirection = currentDirection;
                            lastControlPointPos = newPos;
                        }*/

                        // else: update current curve point
                        //selectedBee.UpdateLastSplinePoint(newPos, currentDirection);
                        selectedBee.AddPathPoint(newPos);
                    }
                    // set last frame values
                    oldPos = newPos;
                    lastFrameDirection = currentDirection;

                }
                else if (newPos != oldPos)
                {
                    //selectedBee.UpdateLastSplinePoint(newPos, (newPos - oldPos).normalized);
                }
            }
            yield return null;
        }
        drawingPath = false;
    }

    IEnumerator Timer()
    {
        while (remainingTime > 0)
        {
            remainingTime -= Time.deltaTime;
            uiTimer.text = Mathf.CeilToInt(remainingTime).ToString();
            yield return null;
        }
    }
}
