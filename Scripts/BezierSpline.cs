﻿using System;
using UnityEngine;

public class BezierSpline : MonoBehaviour
{
    [SerializeField]
    private Vector3[] points;       // array of points (tangent handles + curve handles)

    [SerializeField]
    private BezierControlPointMode[] modes;     // array of control point modes (free, aligned, mirrored)

    private float[] curveStartDistances;

    // number of curves of the spline (1/3 of points)
    public int CurveCount {
        get {
            return (points.Length - 1) / 3;
        }
    }

    // number of points (total)
    public int ControlPointCount {
        get {
            return points.Length;
        }
    }

    // returns specific point
    public Vector3 GetControlPoint(int index) {
        return points[index];
    }

    // set specific point position + changes their tangent positions 
    public void SetControlPoint(int index, Vector3 point) {

        if (index % 3 == 0)
        {
            Vector3 delta = point - points[index];  // difference between old and new point position
            // spline is looping -> first point affects the last & other way around
            if (loop)
            {
                // first point changes [1], [last], [last-1]
                if (index == 0)
                {
                    points[points.Length - 1] = point;
                    points[1] += delta;
                    points[points.Length - 2] += delta;
                }
                // last point changes [last-1], [0], [1]
                else if (index == points.Length - 1)
                {
                    points[0] = point;
                    points[1] += delta;
                    points[index - 1] += delta;
                }
                // normal point changes neighbors
                else {
                    points[index - 1] += delta;
                    points[index + 1] += delta;
                }
            }
            // no loop
            else {
                // if not the first: change the previous point (tangent)
                if (index > 0)
                {
                    points[index - 1] += delta;
                }
                // if not the last: change the next point (tangent)
                if (index + 1 < points.Length)
                {
                    points[index + 1] += delta;
                }
            }
        }
        // set point position
        points[index] = point;
        // apply Control Point Mode
        EnforceMode(index);
    }
    
    // get position of point at length (t between 0 and 1)
    public Vector3 GetPoint(float t)
    {
        int i;          // index of start point of curve
        if (t >= 1f) {
            t = 1f;
            i = points.Length - 4;
        }
        else {
            t = Mathf.Clamp01(t) * CurveCount;
            i = (int)t;
            t -= i;
            i *= 3;
        }
        // return position of point via Bezier.GetPoint (curve start, curve t1, curve t2, curve end)
        return transform.TransformPoint(Bezier.GetPoint(
            points[i], points[i+1], points[i+2], points[i+3], t));
    }

    // get velocity at length (t between 0 and 1)
    public Vector3 GetVelocity(float t)
    {
        int i;
        if (t >= 1f)
        {
            t = 1f;
            i = points.Length - 4;
        }
        else {
            t = Mathf.Clamp01(t) * CurveCount;
            i = (int)t;
            t -= i;
            i *= 3;
        }
        // get velocity via Bezier.GetFirstDerivative
        return transform.TransformPoint(
            Bezier.GetFirstDerivative(
                points[i], points[i+1], points[i+2], points[i+3], t)) - transform.position;
    }

    // reset spline points and control point modes
    public void Reset()
    {
        points = new Vector3[] {
            new Vector3(0f, 0f, 0f),
            new Vector3(0f, 0f, 0f),
            new Vector3(0f, 0f, 0f),
            new Vector3(0f, 0f, 0f)
        };
        modes = new BezierControlPointMode[] {
            BezierControlPointMode.Free,
            BezierControlPointMode.Free
        };
    }

    public void Reset(Vector3 pointA, Vector3 tangentA, Vector3 tangentB, Vector3 pointB)
    {
        points = new Vector3[] {
            pointA,
            tangentA,
            tangentB,
            pointB
        };
        modes = new BezierControlPointMode[] {
            BezierControlPointMode.Aligned,
            BezierControlPointMode.Aligned
        };
    }

    // get direction (normalized velocity) (t between 0 and 1)
    public Vector3 GetDirection(float t)
    {
        return GetVelocity(t).normalized;
    }

    public void AddCurve()
    {
        Vector3 point = points[points.Length - 1];
        Array.Resize(ref points, points.Length + 3);
        point.x += 1f;
        points[points.Length - 3] = point;
        point.x += 1f;
        points[points.Length - 2] = point;
        point.x += 1f;
        points[points.Length - 1] = point;

        Array.Resize(ref modes, modes.Length + 1);
        modes[modes.Length - 1] = modes[modes.Length - 2];
        EnforceMode(points.Length - 4);

        if (loop)
        {
            points[points.Length - 1] = points[0];
            modes[modes.Length - 1] = modes[0];
            EnforceMode(0);
        }
    }

    public BezierControlPointMode GetControlPointMode(int index)
    {
        return modes[(index + 1) / 3];
    }

    public void SetControlPointMode(int index, BezierControlPointMode mode)
    {
        int modeIndex = (index + 1) / 3;
        modes[modeIndex] = mode;
        if (loop)
        {
            if (modeIndex == 0)
            {
                modes[modes.Length - 1] = mode;
            }
            else if (modeIndex == modes.Length - 1)
            {
                modes[0] = mode;
            }
        }
        EnforceMode(index);
    }

    private void EnforceMode(int index)
    {
        int modeIndex = (index + 1) / 3;
        BezierControlPointMode mode = modes[modeIndex];
        if (mode == BezierControlPointMode.Free || !loop && (modeIndex == 0 || modeIndex == modes.Length - 1))
        {
            return;
        }
        int middleIndex = modeIndex * 3;
        int fixedIndex, enforcedIndex;
        if (index <= middleIndex)
        {
            fixedIndex = middleIndex - 1;
            if (fixedIndex < 0)
            {
                fixedIndex = points.Length - 2;
            }
            enforcedIndex = middleIndex + 1;
            if (enforcedIndex >= points.Length)
            {
                enforcedIndex = 1;
            }
        }
        else {
            fixedIndex = middleIndex + 1;
            if (fixedIndex >= points.Length)
            {
                fixedIndex = 1;
            }
            enforcedIndex = middleIndex - 1;
            if (enforcedIndex < 0)
            {
                enforcedIndex = points.Length - 2;
            }
        }
        Vector3 middle = points[middleIndex];
        Vector3 enforcedTangent = middle - points[fixedIndex];
        if (mode == BezierControlPointMode.Aligned)
        {
            enforcedTangent = enforcedTangent.normalized * Vector3.Distance(middle, points[enforcedIndex]);
        }
        points[enforcedIndex] = middle + enforcedTangent;
    }

    [SerializeField]
    private bool loop;

    public bool Loop
    {
        get {
            return loop;
        }
        set {
            loop = value;
            if (value == true)
            {
                modes[modes.Length - 1] = modes[0];
                SetControlPoint(0, points[0]);
            }
        }
    }


    public float GetLength()
    {
        Array.Resize(ref curveStartDistances, CurveCount + 1);
        curveStartDistances[0] = 0;
        float completeLength = 0;
        for (int i = 0; i + 3 < points.Length; i += 3)
        {
            float segmentLength = Bezier.GetQuadraticLength(points[i], Bezier.QuadraticMiddlepoint(points[i], points[i + 1], points[i + 2], points[i + 3]), points[i + 3]);
            completeLength += segmentLength;
            curveStartDistances[i / 3 + 1] = completeLength;
        }
        return completeLength;
    }

    public Vector3 GetPointAtDistance (float distance)
    {
        if (distance > GetLength() || distance < 0)
        {
            return GetControlPoint(ControlPointCount - 1);
        }
        else
        {
            int targetCurveIndex = 0;
            for (int i = 1; i <= CurveCount; i++)
            {
                if (distance < curveStartDistances[i])
                {
                    targetCurveIndex = i - 1;
                    break;
                }
            }
            float t = (distance-curveStartDistances[targetCurveIndex]) / (curveStartDistances[targetCurveIndex + 1] - curveStartDistances[targetCurveIndex]);
            return Bezier.GetPoint(points[targetCurveIndex * 3], points[targetCurveIndex * 3 + 1], points[targetCurveIndex * 3 + 2], points[targetCurveIndex * 3 + 3], t);
        }
    }
}

// modes: 
// free -> tangents free
// aligned -> tangents on one line
// mirrored -> tangents point mirrored at curve point
public enum BezierControlPointMode
{
    Aligned,
    Free,
    Mirrored
}