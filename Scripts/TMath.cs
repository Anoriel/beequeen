﻿using UnityEngine;
using System.Collections;

public static class TMath {

    /// <summary>
    /// returns length of Vector in a specific direction
    /// </summary>
    /// <param name="a">start Vector</param>
    /// <param name="b">end Vector</param>
    /// <param name="direction">direction Vector</param>
    /// <returns>magnitude</returns>
	public static float DistanceInDirection (Vector2 a, Vector2 b, Vector2 direction) {
        return Vector2.Scale((b - a),direction.normalized).magnitude;    
	}

    /// <summary>
    /// absolute value of each Vector component
    /// </summary>
    /// <param name="a">input Vector</param>
    /// <returns>output Vector</returns>
    public static Vector2 VectorAbs (Vector2 a) {
        return new Vector2(Mathf.Abs(a.x), Mathf.Abs(a.y));
    }

    public static float VectorComponentSum (Vector3 a)
    {
        return a.x + a.y + a.z;
    }

    public static float VectorComponentSum(Vector2 a)
    {
        return a.x + a.y;
    }
}
