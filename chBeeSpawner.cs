﻿using UnityEngine;
using System.Collections;

public class chBeeSpawner : MonoBehaviour {

    public chBeeMovement[] beePrefabs;
    
    // Use this for initialization
	void Start () {
        StartCoroutine(SpawnBees());
    }
	
	// Update is called once per frame
	IEnumerator SpawnBees() {
        while (CollectHoneyManager.singleton.remainingTime > 0)
        {
            chBeeMovement newBee = Instantiate(beePrefabs[Random.Range(0, beePrefabs.Length)]);

            Vector2 newBeeDirection = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized;

            newBee.movementDirection = newBeeDirection;

            newBee.transform.position = -5*newBeeDirection;

            yield return new WaitForSeconds(CollectHoneyManager.singleton.beeSpawnRate);
        }
    }
}
