# README #

Am besten arbeitet jeder erstmal in seinen eigenen Szenen und fasst die der anderen nicht an :)

### How do I get set up? ###

Erstellt ein Unityprojekt. Dieses git repository repräsentiert NUR den Asset Ordner, also scenes, scripts, materials etc.
Navigiert dann in den Asset Ordner im eben erstellten Unity Projekt und clont hier dieses repository rein, mit:


```
#!bash

git clone https://Anoriel@bitbucket.org/Anoriel/beequeen.git
```


Danach solltet ihr die Ordnerstruktur und alles bisher gepushte in einem beequeen Ordner im Asset Ordner finden. Wenn ihr wollt könnt ihr den gesamten Inhalt des geklonten Ordners direkt in den Asset Ordner verschieben und den beequeen Ordner löschen, müsst ihr aber nicht :)

#### WICHTIG: Die .gitignore Datei! ####

Die könnt ihr mit einem normalen Texteditor bearbeiten, z.b. Editor in Windows oder Notepad++ oder sowas.
Wenn ihr einen neuen Ordner innerhalb des git repos hinzufügt, MÜSST ihr die gitignore anpassen.
Die .meta Dateien dürfen nicht mit gepusht werden, da es non stop zu merge Konflikten kommen würde.
Damit diese Dateien von git ignoriert werden, den neuen Pfad einfach in einer neuen Zeile hinzufügen, so etwa: 

```
#!bash

PFAD/*.meta (sieht man ja auch an den bereits bestehenden EInträgen in der .gitignore)
```


#### COMMIT + PUSH: ####
Wenn ihr etwas bearbeitet habt und es den anderen verfügbar machen wollt:

```
#!bash

git add . (fügt alle bearbeiteten Dateien für einen commit zusammen, der "." ist wichtig)
```
 

```
#!bash

git commit -m "TRAGT HIER EINE MESSAGE EIN: was ihr gemacht habt, Hinweis etc."
```


```
#!bash

git push origin master (eventuell müsst ihr dann eurer bitbucket Passwort eingeben)
```
 

### Questions? ###

Fragt Nadine :) Lieber einmal mehr fragen, als hinterher das repo fixen müssen ^^